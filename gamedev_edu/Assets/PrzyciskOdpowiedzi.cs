﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrzyciskOdpowiedzi : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => FindObjectOfType<ManagerZadan>().PodajOdpowiedz(GetComponentInChildren<Text>().text));
    }

}

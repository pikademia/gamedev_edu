﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerZadan : MonoBehaviour
{
    [SerializeField] Button[] przyciskiOdpowiedzi;
    [SerializeField] Text txtTrescZadania;
    [SerializeField] Zadanie[] zadania;
    int numerZadania = 0;
    string odpowiedzUzytkownika = "";
    void Start()
    {
        WyswietlZadanie();
    }

    void WyswietlZadanie()
    {
        txtTrescZadania.text = zadania[numerZadania].trescZadania + odpowiedzUzytkownika;
        przyciskiOdpowiedzi[0].GetComponentInChildren<Text>().text = zadania[numerZadania].prawidlowaOdpowiedz;
        przyciskiOdpowiedzi[1].GetComponentInChildren<Text>().text = zadania[numerZadania].zlaOdpowiedz1;
        przyciskiOdpowiedzi[2].GetComponentInChildren<Text>().text = zadania[numerZadania].zlaOdpowiedz2;
    }


    public void PodajOdpowiedz(string odpowiedz)
    {
        odpowiedzUzytkownika = odpowiedz;
        WyswietlZadanie();
    }

    public void SprawdzOdpowiedz()
    {
        if(odpowiedzUzytkownika == zadania[numerZadania].prawidlowaOdpowiedz)
        {
            print("goooood");
            if(numerZadania < zadania.Length - 1)
            {
                numerZadania++;
            }
            else
            {
                numerZadania = 0;
            }
            ResetujOdpowiedzUzytkownika();
            WyswietlZadanie();
        }
        else
        {
            print("nie good");
            ResetujOdpowiedzUzytkownika();
            WyswietlZadanie();
        }
    }

    void ResetujOdpowiedzUzytkownika()
    {
            odpowiedzUzytkownika = "";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Zadanie", menuName = "Utwórz nowe zadanie")]
public class Zadanie : ScriptableObject
{
    public string trescZadania;
    public string prawidlowaOdpowiedz;
    public string zlaOdpowiedz1;
    public string zlaOdpowiedz2;

}
